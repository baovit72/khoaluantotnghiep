import React, {useState, useEffect, useContext} from 'react';
import {
  ApplicationProvider,
  Button,
  Icon,
  Layout,
  Text,
  Input,
  Card,
  List,
  ListItem,
  useTheme,
  Modal,
} from '@ui-kitten/components';
import {
  Dimensions,
  FlatList,
  Image,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';

import EMOJIS from '../../assets/emojis';
import Carousel from 'react-native-snap-carousel';
import Draggable from 'react-native-draggable';
import GlobalContext from '../contexts/global.context';
import {gql, useQuery, useSubscription} from '@apollo/client';
import utils from '../utils';

/**
 * Stylesheet for the component
 */
const styles = StyleSheet.create({
  emotjCard: {
    borderRadius: 15,
    // backgroundColor: 'rgba(0,0,0,0.02)',
    marginBottom: 20,
    flex: 1,
  },
  emotjText: {
    color: 'white',
  },

  contentContainer: {
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderRadius: 15,
    overflow: 'hidden',
    justifyContent: 'center',
  },
  logoTitle: {
    color: '#fff',
    fontSize: 70,
    justifyContent: 'center',
    textAlign: 'center',
    fontFamily: 'LemonJellyPersonalUse',
  },
});

interface IProp {
  navigation: any;
}

export default ({navigation}: IProp) => {
  const {state, dispatch} = useContext(GlobalContext);

  const [thoughtModal, setThoughtModal] = useState(false);
  const theme = useTheme();
  const t_backgroundColor = theme['background-basic-color-1'];
  interface IRProp {
    item: any;
    index: any;
  }
  const _renderEmojiCard = ({item, index}) => {
    let meStyle = {};
    if (item.name === 'Me') {
      meStyle = {borderWidth: 1, borderColor: theme['color-primary-400']};
    }
    return (
      <Card
        style={{
          ...meStyle,
          ...styles.emotjCard,
          padding: 0,
          // borderColor: theme['color-primary-400'],
          // backgroundColor: theme['color-primary-200'],
        }}>
        {/* {state.group && (
          <Draggable
            x={200}
            y={300}
            renderColor={theme['color-primary-400']}
            renderText={'dksljlsjdlskjd'}
          />
        )} */}
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: -17,
            }}>
            <Image
              style={{
                width: 50,
                height: 50,
                borderRadius: 25,
                marginBottom: 5,
              }}
              source={{uri: JSON.parse(item.user).avatar}}
            />
            <Text style={{fontWeight: 'bold'}}>
              {JSON.parse(item.user).name}
            </Text>
          </View>
          <View
            style={{
              flex: 2,
            }}>
            <Image
              source={EMOJIS[`${item.emoticonId}_PNG`]}
              style={{
                position: 'absolute',
                width: 150,
                height: 150,
                opacity: 0.7,
                right: -57,
                bottom: -57,
                transform: [{rotate: '-15deg'}],
              }}></Image>
            <Text
              style={{
                flex: 1,
                zIndex: 2,
                // color: theme['color-primary-400'],
                fontWeight: '600',
                color: 'gray',
              }}>
              {item.content}
            </Text>
          </View>
        </View>
      </Card>
    );
  };
  const _renderItem = ({item, index}: IRProp) => {
    console.log(item);
    return (
      <TouchableOpacity
        style={{width: 70, height: '100%', flex: 1}}
        activeOpacity={1}
        onPress={() => {
          setSelectedEmo(item);
          setIsMakingThought(true);
          setThoughtModal(true);
        }}>
        <Image
          style={{width: 75, height: '100%', flex: 1, marginBottom: 20}}
          source={EMOJIS[item + '_GIF']}
        />
      </TouchableOpacity>
    );
  };
  const [userThought, setUserThought] = useState({
    avtUrl: state.user.avatar,
    name: 'Me',
    content: '',
    emo: 'THUMBS_UP',
    backgroundColor: '',
  });

  const [isMakingThought, setIsMakingThought] = useState(false);
  const [selectedEmo, setSelectedEmo] = useState(false);
  const [content, setContent] = useState('');
  const [thoughtContent, setThoughtContent] = useState('');

  const [feelings, setFeelings] = useState([]);
  const GET_MESSAGES = gql`
    query {
      feeling {
        content
        emoticonId
        user
        id
      }
    }
  `;
  const POLL_MESSAGES = gql`
    subscription {
      diarySubscription {
        description
        image
        createdAt
        user
      }
    }
  `;
  const {
    data: queryMsg,
    refetch,
    loading,
  } = useQuery(GET_MESSAGES, {pollInterval: 500});
  // const {data: pollMsg, error} = useSubscription(POLL_MESSAGES, {
  //   variables: {token: 'Bearer ' + state.token},
  // });
  // useEffect(() => {
  //   console.log(pollMsg, error);
  //   if (!pollMsg) return;
  //   const diaryItemPoll = pollMsg.diarySubscription;
  //   setDiaryItems([...diaryItems, diaryItemPoll]);
  // }, [pollMsg, error]);
  useEffect(() => {
    if (!queryMsg) return;
    const feelings = queryMsg.feeling;
    setFeelings(feelings);
    console.log(feelings);
  }, [queryMsg]);

  return (
    <View
      style={{
        width: '100%',
        height: '100%',
        flex: 1,
        backgroundColor: t_backgroundColor,
      }}>
      <View
        style={{
          backgroundColor: theme['color-primary-400'],
          height: 150,
          justifyContent: 'center',
          alignItems: 'center',
          // paddingBottom: 20,
          borderBottomLeftRadius: 50,
          overflow: 'hidden',
        }}>
        <View
          style={{
            marginTop: 10,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            alignContent: 'center',
          }}>
          <Text
            style={{
              color: t_backgroundColor,
              fontSize: 25,
              fontFamily: 'BubbleBobble',
            }}>
            I feel...
          </Text>
        </View>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.contentContainer}
          style={{height: 75, marginTop: 5}}
          data={[
            'BIRTHDAY_CAKE',
            'ANGRY_EMOJI',
            'BLUSHING_EMOJI',
            'HEART',
            'HEARTEYE_EMOJI',
            'LAUGHING_EMOJI',
            // 'MEDAL',
            'PEACE',
            'SAD_EMOJI',
            'SAD_EMOJI_WITH_TEAR',
            'TEARS_OF_JOY',
          ]}
          renderItem={_renderItem}
        />
      </View>
      <View
        style={{
          flex: 7,
          backgroundColor: theme['color-primary-400'],
        }}>
        <View
          style={{
            borderTopRightRadius: 50,
            backgroundColor: t_backgroundColor,
            flex: 1,
            paddingLeft: 20,
            paddingRight: 20,
            flexDirection: 'column',
            paddingTop: 30,
          }}>
          {/* <Image source={require('./../../assets/emojis/BIRTHDAY_CAKEx.gif')} /> */}
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 20,
            }}>
            <Text category="h5" style={{color: theme['color-primary-400']}}>
              today feelings
            </Text>
          </View>
          <FlatList
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.contentContainer}
            style={{...styles.container}}
            data={feelings}
            renderItem={_renderEmojiCard}
          />

          {/* <Carousel
        data={[
          'BIRTHDAY_CAKE.gif',
          'ANGRY_EMOJI',
          'BLUSHING_EMOJI',
          'HEART',
          'HEARTEYE_EMOJI',
          'LAUGHING_EMOJI',
          'MEDAL',
          'PEACE',
          'SAD_EMOJI',
          'SAD_EMOJI_WITH_TEAR',
          'TEARS_OF_JOY',
        ]}
        renderItem={_renderItem}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={Dimensions.get('window').width}
        activeSlideAlignment={'start'}
        inactiveSlideScale={1}
        inactiveSlideOpacity={1}
      /> */}
        </View>
      </View>

      <Modal
        visible={thoughtModal}
        onBackdropPress={() => setThoughtModal(false)}
        style={{
          width: '85%',
          borderRadius: 30,

          borderWidth: 0,

          paddingBottom: 50,
        }}
        backdropStyle={{backgroundColor: 'rgba(0, 0, 0, 0.5)'}}
        // onBackdropPress={() => setVisible(false)}
      >
        <Card
          style={{
            borderRadius: 30,
            backgroundColor: theme['color-primary-400'],
            borderWidth: 0,
            paddingTop: 0,
          }}
          disabled={true}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Input
              value={thoughtContent}
              onChangeText={setThoughtContent}
              multiline
              textStyle={{
                height: 120,
                borderRadius: 30,
                fontSize: 20,
              }}
              placeholder={'write down your thoughts...'}
              style={{
                marginTop: 0,
                flex: 5,
                borderRadius: 30,
              }}></Input>
            <Image
              source={EMOJIS[`${selectedEmo}_PNG`]}
              style={{
                position: 'absolute',
                width: 50,
                height: 50,
                opacity: 0.5,
                right: 0,
                bottom: 0,
                zIndex: 2,
                transform: [{rotate: '-15deg'}],
              }}></Image>
            <Button
              appearance="outline"
              status="control"
              accessoryLeft={props => <Icon {...props} name="edit-outline" />}
              style={{flex: 1, borderRadius: 30, height: '95%', marginLeft: 10}}
              onPress={() => {
                utils.sendFeeling(state.token, {
                  emoticon: selectedEmo,
                  content: thoughtContent,
                });
                // setUserThought({
                //   avtUrl: state.user.avatar,
                //   name: 'Me',
                //   content: thoughtContent,
                //   emo: selectedEmo,
                //   backgroundColor: '',
                // });
                setThoughtModal(false);
              }}></Button>
          </View>
        </Card>
        <Button
          style={{
            position: 'absolute',
            left: Dimensions.get('window').width / 2 - 50,
            bottom: -25,
            width: 50,
            height: 50,
            borderRadius: 25,
            borderWidth: 0,
          }}
          appearance="outline"
          status="control"
          accessoryLeft={props => <Icon {...props} name="close-outline" />}
          onPress={() => setThoughtModal(false)}></Button>
      </Modal>
    </View>
  );
};
